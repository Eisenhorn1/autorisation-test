var form = document.getElementById('Form');
var url = 'http://test.kluatr.ru/api/user/login';

form.onsubmit = function(event) {
  event.preventDefault();
  var formData = new FormData(form);
  var xhr = new XMLHttpRequest();
  xhr.open('POST', url);
  
  xhr.addEventListener('readystatechange', function() {
    // если все хорошо
    if (this.readyState == 4 && this.status == 200) {
      // разбираем строку json, помещаем её в переменную data
      var data = JSON.parse(this.responseText).data;
      // создаём переменную, в которую будем складывать результат работы (маркированный список)
      var output = '<ul>';
      // переберём объект data
      for (var key in data) {
        output += '<li><b>' + key + "</b>: " + data[key] + '</li>';
      }
      // добавим к переменной закрывающий тег ul
      output += '</ul>';
      // выведем в элемент (id = "result") значение переменной output
      document.getElementById('result').innerHTML = output;

    }
  });

  xhr.send(formData);  
}
